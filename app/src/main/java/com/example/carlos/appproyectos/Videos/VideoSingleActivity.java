package com.example.carlos.appproyectos.Videos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.carlos.appproyectos.R;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class VideoSingleActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener{

    private String mPost_key = null;

    private DatabaseReference mDatabase;
    private YouTubePlayerView mNoticiasSingleImage;
    private YouTubePlayer.OnInitializedListener onInitializedListener;
    private TextView mNoticiasSingleTitle;
    private TextView mNoticiasSingleConsejo;
    private String post_vide;


    public static final  String  API_KEY = "AIzaSyC42HX8X7zhrhJ15Mrar3xQgYIJXmiQDq8";
    public static final  String  VIDEO_ID = "SvVcGPwKuI4";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_single);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Videos");
        mPost_key = getIntent().getExtras().getString("blog_id");

        mNoticiasSingleImage = (YouTubePlayerView) findViewById(R.id.videoView);
        //mNoticiasSingleImage.initialize(API_KEY,this);
        mNoticiasSingleTitle = (TextView)findViewById(R.id.post_titleNoti);
        mNoticiasSingleConsejo = (TextView)findViewById(R.id.post_ConsejoNoti);


        // Toast.makeText(NoticiasSingleActivity.this,post_key, Toast.LENGTH_LONG).show();
        mDatabase.child(mPost_key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String post_title = (String)dataSnapshot.child("title").getValue();
                final String post_vide = (String)dataSnapshot.child("image").getValue();
                String post_consejo = (String)dataSnapshot.child("desc").getValue();

                mNoticiasSingleTitle.setText(post_title);
                mNoticiasSingleConsejo.setText(post_consejo);

                mNoticiasSingleImage.initialize(API_KEY,
                        new YouTubePlayer.OnInitializedListener() {
                            @Override
                            public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                                YouTubePlayer youTubePlayer, boolean b) {
                                youTubePlayer.cueVideo(post_vide);
                            }
                            @Override
                            public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                                YouTubeInitializationResult youTubeInitializationResult) {

                            }
                        });





            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }
}
