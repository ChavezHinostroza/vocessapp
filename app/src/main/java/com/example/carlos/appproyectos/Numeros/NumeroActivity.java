package com.example.carlos.appproyectos.Numeros;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.carlos.appproyectos.DiasSemana.Semana;
import com.example.carlos.appproyectos.DiasSemana.SemanaActivity;
import com.example.carlos.appproyectos.DiasSemana.SemanaSingleActivity;
import com.example.carlos.appproyectos.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class NumeroActivity extends AppCompatActivity {

    private RecyclerView mNoticiasList;
    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numero);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Numero");

        mDatabase.keepSynced(true);

        mNoticiasList= (RecyclerView) findViewById(R.id.blog_listnumeros);
        mNoticiasList.setHasFixedSize(true);
        mNoticiasList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<Numero, BlogViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Numero, NumeroActivity.BlogViewHolder>(

                Numero.class,
                R.layout.blow_rownumber,
                NumeroActivity.BlogViewHolder.class,
                mDatabase

        ) {
            @Override
            protected void populateViewHolder(NumeroActivity.BlogViewHolder viewHolder, Numero model, int position) {

                final String post_key = getRef(position).getKey();
                viewHolder.setTitle(model.getTitle());
                viewHolder.setDesc(model.getDesc());
                viewHolder.setImage(getApplicationContext(), model.getImage());
               // viewHolder.setImage(getApplicationContext(), model.getImage2());


                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent IntenNotSingle = new Intent(NumeroActivity.this, NumeroSingleActivity.class);
                        IntenNotSingle.putExtra("blog_id",post_key);
                        startActivity(IntenNotSingle);


                    }
                });

            }
        };

        mNoticiasList.setAdapter(firebaseRecyclerAdapter);

    }

    public static class BlogViewHolder extends RecyclerView.ViewHolder{

        View mView;

        public BlogViewHolder(View itemView) {
            super(itemView);

            mView=itemView;

        }


        public  void  setTitle(String title){
            TextView post_title = (TextView) mView.findViewById(R.id.post_title12);
            post_title.setText(title);
        }

        public  void  setDesc(String desc){
             TextView post_desc = (TextView) mView.findViewById(R.id.post_desc12);
             post_desc.setText(desc);
        }

        public void setImage(final Context ctx, final String image){

             ImageView post_image = (ImageView)  mView.findViewById(R.id.post_image12);
             Picasso.with(ctx).load(image).into(post_image);

        }
    }
}
