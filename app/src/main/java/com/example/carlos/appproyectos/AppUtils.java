package com.example.carlos.appproyectos;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by andres on 14/11/2016.
 */
public class AppUtils {
    public  static void showToast(Context context, String message){
        Toast.makeText(context,message, Toast.LENGTH_SHORT).show();
    }
}
