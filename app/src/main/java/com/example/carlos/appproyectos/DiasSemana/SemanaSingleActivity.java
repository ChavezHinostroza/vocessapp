package com.example.carlos.appproyectos.DiasSemana;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.carlos.appproyectos.R;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class SemanaSingleActivity extends AppCompatActivity {

    private String mPost_key = null;
    private DatabaseReference mDatabase;
    private TextView mNoticiasSingleTitle;
    //private String post_vide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_semana_single);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Semana");
        mPost_key = getIntent().getExtras().getString("blog_id");
        mNoticiasSingleTitle = (TextView)findViewById(R.id.mensajedia);
      final ImageView mNoticiasSingleImage = (ImageView) findViewById(R.id.imgdiadesemana);
     final  ImageView mNoticiasSingleImage1 = (ImageView) findViewById(R.id.diadesemana);

        mDatabase.child(mPost_key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

               // String post_title = (String)dataSnapshot.child("title").getValue();
                String post_desc = (String)dataSnapshot.child("desc").getValue();
                String post_img = (String)dataSnapshot.child("img").getValue();
             //   String post_img2 = (String)dataSnapshot.child("img2").getValue();


               // mNoticiasSingleTitle.setText(post_title);
                mNoticiasSingleTitle.setText(post_desc);
                Picasso.with(SemanaSingleActivity.this).load(post_img).into(mNoticiasSingleImage);
             //   Picasso.with(SemanaSingleActivity.this).load(post_img2).into(mNoticiasSingleImage1);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
