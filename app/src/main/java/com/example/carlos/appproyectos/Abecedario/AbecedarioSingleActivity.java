package com.example.carlos.appproyectos.Abecedario;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.carlos.appproyectos.Numeros.NumeroSingleActivity;
import com.example.carlos.appproyectos.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class AbecedarioSingleActivity extends AppCompatActivity {

    private String mPost_key = null;
    private DatabaseReference mDatabase;
    private TextView mNoticiasSingleTitle;
    private ImageView mImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abecedario_single);


        mDatabase = FirebaseDatabase.getInstance().getReference().child("Abecedario");
        mPost_key = getIntent().getExtras().getString("blog_id");
        mNoticiasSingleTitle = (TextView)findViewById(R.id.mensajedia);
        mImage = (ImageView) findViewById(R.id.imgdiadesemana);

        mDatabase.child(mPost_key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String post_title = (String)dataSnapshot.child("title").getValue();
                // String post_img = (String)dataSnapshot.child("image").getValue();
                String post_img2 = (String)dataSnapshot.child("img2").getValue();


                mNoticiasSingleTitle.setText(post_title);
                Picasso.with(AbecedarioSingleActivity.this).load(post_img2).into(mImage);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
