package com.example.carlos.appproyectos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class DetalleDiaSemana extends AppCompatActivity {

    String lunes1, martes2, miercoles3, jueves4,viernes5,sabado6,domingo7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_dia_semana);

        //traer valor de UserId
        lunes1 = getIntent().getExtras().getString("lunes");
        martes2 = getIntent().getExtras().getString("martes");
        miercoles3 = getIntent().getExtras().getString("miercoles");
        jueves4 = getIntent().getExtras().getString("jueves");
        viernes5 = getIntent().getExtras().getString("viernes");
        sabado6 = getIntent().getExtras().getString("sabado");
        domingo7 = getIntent().getExtras().getString("domingo");

        ImageView imageView = (ImageView) findViewById(R.id.imgdiadesemana);
        ImageView imageView2 = (ImageView) findViewById(R.id.diadesemana);

        if(lunes1 == "lunes"){
            imageView.setImageResource(R.drawable.alunes1);
            imageView2.setImageResource(R.drawable.alunes2);
        }
        if(martes2 == "martes"){
            imageView.setImageResource(R.drawable.bmartes1);
            imageView2.setImageResource(R.drawable.bmartes2);
        }

    }
}
