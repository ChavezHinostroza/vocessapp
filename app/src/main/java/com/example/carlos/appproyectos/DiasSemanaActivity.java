package com.example.carlos.appproyectos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class DiasSemanaActivity extends AppCompatActivity {

    ImageButton lunes, martes, miercoles, jueves,viernes,sabado,domingo;
    String lunes1, martes2, miercoles3, jueves4,viernes5,sabado6,domingo7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dias_semana);

        lunes = (ImageButton)findViewById(R.id.imageViewds);
        martes = (ImageButton)findViewById(R.id.imageViewds2);
        miercoles = (ImageButton)findViewById(R.id.imageViewds3);
        jueves = (ImageButton)findViewById(R.id.imageViewds4);
        viernes = (ImageButton)findViewById(R.id.imageViewds5);
        sabado = (ImageButton)findViewById(R.id.imageViewds6);
        domingo = (ImageButton)findViewById(R.id.imageViewds7);


        lunes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(DiasSemanaActivity.this,DetalleDiaSemana.class);
                intent.putExtra("lunes", lunes1);
                startActivity(intent);
            }
        });
        martes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(DiasSemanaActivity.this,DetalleDiaSemana.class);
                intent.putExtra("martes", martes2);
                startActivity(intent);
            }
        });
        miercoles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(DiasSemanaActivity.this,DetalleDiaSemana.class);
                intent.putExtra("miercoles", miercoles3);
                startActivity(intent);
            }
        });
        jueves.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(DiasSemanaActivity.this,DetalleDiaSemana.class);
                intent.putExtra("jueves", jueves4);
                startActivity(intent);
            }
        });
        viernes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(DiasSemanaActivity.this,DetalleDiaSemana.class);
                intent.putExtra("viernes", viernes5);
                startActivity(intent);
            }
        });
        sabado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(DiasSemanaActivity.this,DetalleDiaSemana.class);
                intent.putExtra("sabado", sabado6);
                startActivity(intent);
            }
        });
        domingo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(DiasSemanaActivity.this,DetalleDiaSemana.class);
                intent.putExtra("domingo", domingo7);
                startActivity(intent);
            }
        });

    }
}
