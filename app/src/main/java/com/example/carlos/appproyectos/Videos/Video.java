package com.example.carlos.appproyectos.Videos;

/**
 *
 */

public class Video {

    private String title, desc, image;

    public Video() {
    }

    public Video(String title, String desc, String image) {
        this.title = title;
        this.desc = desc;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String titleNot) {
        this.title = titleNot;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String descNot) {
        this.desc = descNot;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String imageNot) {
        this.image = imageNot;
    }
}
