package com.example.carlos.appproyectos;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.preference.PreferenceFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.example.carlos.appproyectos.Abecedario.AbecedarioActivity;
import com.example.carlos.appproyectos.DiasSemana.SemanaActivity;
import com.example.carlos.appproyectos.Meses.MesesActivity;
import com.example.carlos.appproyectos.Numeros.NumeroActivity;

public class InterpretaActivity extends AppCompatActivity {

    Button btnInterpretar,btnletras,diasemana,animales,colores,mesesdeano;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interpreta);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.contenedor_configuracion, new InterpretaActivity.FragmentoConfiguracion());
        ft.commit();
        agregarToolbar();

        btnInterpretar = (Button)findViewById(R.id.btn_familia);
        btnInterpretar.getBackground().setAlpha(158);

        btnletras = (Button)findViewById(R.id.btn_adivina);
        btnletras.getBackground().setAlpha(158);

        diasemana = (Button)findViewById(R.id.btn_diasemana);
        diasemana.getBackground().setAlpha(158);

        animales = (Button)findViewById(R.id.btn_camara);
        animales.getBackground().setAlpha(158);

        colores = (Button)findViewById(R.id.btn_ANIMALES);
        colores.getBackground().setAlpha(158);

        mesesdeano = (Button)findViewById(R.id.btn_interpreta);
        mesesdeano.getBackground().setAlpha(158);

        diasemana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(InterpretaActivity.this,SemanaActivity.class);
                startActivity(intent);
            }
        });

        btnletras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(InterpretaActivity.this,NumeroActivity.class);
                startActivity(intent);
            }
        });

        btnInterpretar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(InterpretaActivity.this,AbecedarioActivity.class);
                startActivity(intent);
            }
        });

        mesesdeano.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(InterpretaActivity.this,MesesActivity.class);
                startActivity(intent);
            }
        });

    }

    private void agregarToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Vocabulario");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final ActionBar ab = getSupportActionBar();

        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    public static class FragmentoConfiguracion extends PreferenceFragment {

        public FragmentoConfiguracion() {
            // Constructor Por Defecto
        }

    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }
}
