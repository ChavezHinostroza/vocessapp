package com.example.carlos.appproyectos;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by andres on 14/11/2016.
 */
public class ListaAdapter extends BaseAdapter {

    private ArrayList<ListaItem> mItemsList;
    private LayoutInflater mLayoutInflater;
    private OnFruitItemClickListener listener;
    private ArrayList<ListaItem> filteredData;
    private ItemFilter mFilter = new ItemFilter();

    public ListaAdapter(Context context, ArrayList<ListaItem> list) {
        mItemsList = list;
        this.filteredData = list;
        mLayoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public void setItemlist(ArrayList<ListaItem> list) {
        mItemsList = list;
        filteredData = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return  filteredData.size();
      /*  if (mItemsList != null) {
            return mItemsList.size();
        } else {
            return 0;
        } */
    }

    @Override
    public ListaItem getItem(int position) {
        return  filteredData.get(position);
       /* ListaItem item = null;
        if (mItemsList != null && mItemsList.size() > 0) {
            item = mItemsList.get(position);
        }
        return item;*/
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setOnFruitClickListener(OnFruitItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnFruitItemClickListener {

        public void onCheckboxClicked(int position, ListaItem item);

    }

    public ArrayList<ListaItem> getUpdatedList() {
        return mItemsList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            ViewHolder viewHolder = new ViewHolder();
            view = mLayoutInflater.inflate(R.layout.select_item, parent,
                    false);
            viewHolder.header_textview = (TextView) view
                    .findViewById(R.id.header_textview);
            viewHolder.sub_textview = (TextView) view
                    .findViewById(R.id.sub_textview);

            view.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        final ListaItem item = getItem(position);
        if (item != null && viewHolder != null) {
            viewHolder.header_textview.setText(item.getName());
            viewHolder.sub_textview.setText(item.getMensage());

        }
        return view;
    }

    public static class ViewHolder {
        TextView header_textview;
        TextView sub_textview;


    }

   public Filter getFilter(){return mFilter;}

    private class  ItemFilter extends Filter {

        @Override
        protected Filter.FilterResults performFiltering(CharSequence constraint) {
            Log.i("TAG_FILTRO", String.valueOf(constraint));
            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<ListaItem> list = mItemsList;

            int count = list.size();
            final ArrayList<ListaItem> nlist = new ArrayList<ListaItem>(count);

            String filterableString ;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getName();
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(list.get(i));
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<ListaItem>) results.values;
            notifyDataSetChanged();
        }


    }
}
