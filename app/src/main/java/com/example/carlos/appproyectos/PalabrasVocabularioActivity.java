package com.example.carlos.appproyectos;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class PalabrasVocabularioActivity extends AppCompatActivity {

    private Context mContext;
    private ListaAdapter mFruitListAdapter;
    private ArrayList<ListaItem> mFruitList;
    private ListView mFruitListview;
    private EditText buscar;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_palabras_vocabulario);

        mContext = PalabrasVocabularioActivity.this;
        initView();
        mFruitList = ListaDummyDataManager.getFruitItemList();
        populateFruitList(mFruitList);
        mFruitListview = (ListView) findViewById(R.id.fruit_listview);
        buscar =(EditText) findViewById(R.id.buscar);

        mFruitListview.setAdapter(mFruitListAdapter);
        buscar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mFruitListAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void  initView(){
        mFruitListview = (ListView) findViewById(R.id.fruit_listview);

        mFruitListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpView, View view,
                                    int position, long itemId) {
                Log.d("TEST", "onItemClick");
                if (mFruitListAdapter != null) {
                    ListaItem item = mFruitListAdapter.getItem(position);
                    AppUtils.showToast(mContext, getString(R.string.item_click)
                            + item.getName());
                }
            }
        });
    }
    public void viewClickListener(View view) {
        if (R.id.back_arrow_view == view.getId()) {
            finish();
        }
    }

    private ListaAdapter.OnFruitItemClickListener listListener = new ListaAdapter.OnFruitItemClickListener() {

        @Override
        public void onCheckboxClicked(int position, ListaItem item) {


        }

    };

    private void populateFruitList(ArrayList<ListaItem> list) {
        if (list != null && list.size() > 0) {
            if (mFruitListAdapter == null) {
                mFruitListAdapter = new ListaAdapter(mContext, mFruitList);
                mFruitListAdapter.setOnFruitClickListener(listListener);
            }
            mFruitListview.setAdapter(mFruitListAdapter);
        } else {
            mFruitListAdapter.setItemlist(mFruitList);
            AppUtils.showToast(mContext,
                    getString(R.string.no_message_list_found));
        }
    }

    @Override
    protected void onDestroy() {
        if (mContext != null) {
            mFruitListAdapter = null;
            mFruitList = null;
            mFruitListview = null;
            mContext = null;
            super.onDestroy();
        }
    }
}
