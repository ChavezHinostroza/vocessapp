package com.example.carlos.appproyectos;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.carlos.appproyectos.Videos.VideoActivity;

public class VocabularioMenuActivity extends AppCompatActivity {


    ImageButton btnbasico, btnintermedio, btnavanzado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vocabulario_menu);

        btnbasico = (ImageButton)findViewById(R.id.ibbasico);
        btnintermedio = (ImageButton)findViewById(R.id.ibintermedio);
        btnavanzado = (ImageButton)findViewById(R.id.ibavanzado);


        btnbasico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(VocabularioMenuActivity.this,VocabularioActivity.class);
                startActivity(intent);
            }
        });

        btnintermedio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(VocabularioMenuActivity.this,BorrarLuegoActivity.class);
                startActivity(intent);
            }
        });

        btnavanzado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(VocabularioMenuActivity.this,VideoActivity.class);
                startActivity(intent);
            }
        });



    }
}
