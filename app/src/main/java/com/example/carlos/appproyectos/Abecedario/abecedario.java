package com.example.carlos.appproyectos.Abecedario;

/**
 * Created by Carlos on 14/07/2017.
 */

public class abecedario {
    private String title, desc, image, image2;

    public abecedario() {
    }

    public abecedario(String title, String desc, String image, String image2) {
        this.title = title;
        this.desc = desc;
        this.image = image;
        this.image2= image2;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
