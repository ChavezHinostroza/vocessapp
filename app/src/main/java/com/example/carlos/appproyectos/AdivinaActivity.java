package com.example.carlos.appproyectos;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class AdivinaActivity extends AppCompatActivity {

   public int scoreCount;
    TextView scoreView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adivina);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.contenedor_configuracion, new AdivinaActivity.FragmentoConfiguracion());
        ft.commit();
        agregarToolbar();

        final  Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        final MediaPlayer  mp =MediaPlayer.create(this,R.raw.applause4);
        //v.vibrate(1000);

        final ImageButton button = (ImageButton) findViewById(R.id.img_abuelo);

        scoreCount = 3;

        this.scoreView = (TextView) findViewById(R.id.tvscore);
        scoreView.setText("Puntaje: " + scoreCount);


        button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    v.vibrate(500);
                   // button.setBackgroundColor(Color.RED);
                   // button.setText("Error");
                    downScore();
                   // score.setText("Puntaje: 20");
                }
                return false;
            }

        });

        final ImageButton button2 = (ImageButton) findViewById(R.id.img_hijo);
        button2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    v.vibrate(500);
               //     button2.setBackgroundColor(Color.RED);
                 //   button2.setText("Error");
                    downScore();
                   // score.setText("Puntaje: 10");
                }
                return false;
            }

        });

        final ImageButton button3 = (ImageButton) findViewById(R.id.img_padre);
        button3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    mp.start();
                  //  button3.setBackgroundColor(Color.GREEN);
                  //  button3.setText("Correcto");
                    upScore();
                }
                return false;
            }

        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                button3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(AdivinaActivity.this,AdivinaActivity2.class);
                        intent.putExtra("userId", scoreCount);
                        startActivity(intent);
                    }
                });
            }
        }, 2000);

    }

    public void upScore(){
        scoreCount++;
        scoreView.setText("Puntaje: " + scoreCount);
    }

    public void downScore(){
        scoreCount--;
        scoreView.setText("Puntaje: " + scoreCount);
    }

    private void agregarToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Adivina");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final ActionBar ab = getSupportActionBar();

        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    public static class FragmentoConfiguracion extends PreferenceFragment {

        public FragmentoConfiguracion() {
            // Constructor Por Defecto
        }

    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }
}
