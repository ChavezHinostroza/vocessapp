package com.example.carlos.appproyectos;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class CamaraActivity extends AppCompatActivity {

    ImageView imgimagen;
    Button btnFoto;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camara);
        /** FragmentTransaction ft = getFragmentManager().beginTransaction();
         ft.add(R.id.contenedor_configuracion, new MainActivity.FragmentoConfiguracion());
         ft.commit();
         agregarToolbar();*/

        btnFoto = (Button)findViewById(R.id.btnfoto);
        imgimagen = (ImageView)findViewById(R.id.imgfoto);

        btnFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LlamarIntent();
            }
        });

    }



    private void LlamarIntent(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imgimagen.setImageBitmap(imageBitmap);
        }
    }

/**
 private void agregarToolbar() {
 Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
 setSupportActionBar(toolbar);

 getSupportActionBar().setTitle("Cámara");
 getSupportActionBar().setDisplayHomeAsUpEnabled(true);

 final ActionBar ab = getSupportActionBar();

 if (ab != null) {
 ab.setDisplayHomeAsUpEnabled(true);
 }
 }

 public static class FragmentoConfiguracion extends PreferenceFragment {

 public FragmentoConfiguracion() {
 // Constructor Por Defecto
 }

 }

 public boolean onSupportNavigateUp() {
 onBackPressed();
 return false;
 }*/

}
