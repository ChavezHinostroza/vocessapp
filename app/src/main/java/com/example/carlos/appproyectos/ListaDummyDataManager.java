package com.example.carlos.appproyectos;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by andres on 14/11/2016.
 */
public class ListaDummyDataManager {


    public static final String[] titles = new String[] { "Hola", "Muy satisfecho",
            "Gracias", "Perdon", "Un Gusto conocerte", "Adios", "Sueño",
            "Sorprendido" };
    public static final String[] messages = new String[] {
            "Vocabulario Basico 1",
            "Vocabulario Basico 2",
            "Vocabulario Basico 1",
            "Vocabulario Basico 1",
            "Vocabulario Basico 2",
            "Vocabulario Basico 1",
            "Vocabulario Basico 1",
            "Vocabulario Basico 1" };

    public static ArrayList<ListaItem> getFruitItemList() {
        ArrayList<ListaItem> list = new ArrayList<ListaItem>();
        for (int i = 0; i < titles.length; i++) {
            ListaItem item = new ListaItem();
            item.setName(titles[i]);
            item.setMensage(messages[i]);
            list.add(item);

        }
        return list;
    }

    public static ListaItem getRandomItem() {
        ListaItem item = new ListaItem();
        int randomValue = new Random().nextInt(titles.length);
        item.setName(titles[randomValue]);
        item.setMensage(messages[randomValue]);
        return item;
    }
}
