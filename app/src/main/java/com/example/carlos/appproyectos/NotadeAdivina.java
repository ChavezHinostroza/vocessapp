package com.example.carlos.appproyectos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class NotadeAdivina extends AppCompatActivity {

    public int scoreCount,premio;
    TextView scoreView,mensaje;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notade_adivina);

        Button exit = (Button)findViewById(R.id.salir);

        premio=8;
        //traer valor de UserId
        scoreCount = getIntent().getExtras().getInt("userId");

        this.scoreView = (TextView) findViewById(R.id.tvscore);
        scoreView.setText("Puntaje: " + scoreCount);

        mensaje = (TextView)findViewById(R.id.mensaje);
        ImageView imageView = (ImageView) findViewById(R.id.post_imageAlim5);

        if (scoreCount >= premio){
            mensaje.setText("Felicidades has logrado completar este nivel!!!");
            imageView.setImageResource(R.drawable.ganador);

        }else{
            mensaje.setText("Lo has hecho bien pero no lo suficiente,sigue intentando");
            imageView.setImageResource(R.drawable.tryagain);
        }



        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(NotadeAdivina.this,MainActivity.class);
                startActivity(intent);
            }
        });



    }
}
