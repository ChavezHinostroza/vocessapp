package com.example.carlos.appproyectos;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.carlos.appproyectos.Videos.VideoActivity;

public class MainActivity extends AppCompatActivity {
    Button btnVocabulario, btnAdivina, btnInterpreta, btnCamara;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnVocabulario = (Button)findViewById(R.id.btn_vocabulario);
        btnVocabulario.getBackground().setAlpha(158);
        btnAdivina = (Button)findViewById(R.id.btn_adivina);
        btnAdivina.getBackground().setAlpha(158);
        btnInterpreta = (Button)findViewById(R.id.btn_interpreta);
        btnInterpreta.getBackground().setAlpha(158);
        btnCamara = (Button)findViewById(R.id.btn_camara);
        btnCamara.getBackground().setAlpha(158);
      //  ImageButton btnListaVoca = (ImageButton)findViewById(R.id.post_imageAlim5);
     /**   btnListaVoca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,PalabrasVocabularioActivity.class);
                startActivity(intent);
            }
        });
*/

        btnVocabulario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,InterpretaActivity.class);
                startActivity(intent);

            }
        });


        btnAdivina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,AdivinaActivity.class);
                startActivity(intent);
            }
        });

        btnInterpreta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,VideoActivity.class);
                startActivity(intent);
            }
        });


        btnCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,CamaraActivity.class);
                startActivity(intent);
            }
        });


    }

}
