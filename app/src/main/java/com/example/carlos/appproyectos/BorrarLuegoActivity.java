package com.example.carlos.appproyectos;

import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.Random;

public class BorrarLuegoActivity extends AppCompatActivity {

    int scoreCount;
    TextView scoreView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borrar_luego);

        scoreCount = 30;

        this.scoreView = (TextView) findViewById(R.id.score);
        scoreView.setText("Score: " + scoreCount);

    }




    public void outcome(View view){
        //This function will determine whether the user picked Rock, Paper, or Scissors
        //and then randomly have the computer pick Rock, Paper, or Scissors. A dialog will then
        //generate to let the user know whether they won or lost.

        String[] rpsArray = {"Rock","Paper","Scissors"};
        int index = new Random().nextInt(rpsArray.length);
        String cpuChoice = rpsArray[index];
        String message = "";

        int id = view.getId();

        switch(id){
            case R.id.rock_button:
                if(cpuChoice.equals("Rock")){
                    //Tie
                    Toast.makeText(this,"The Computer picked Rock! It's a Tie!",Toast.LENGTH_SHORT).show();

                }
                else if(cpuChoice.equals("Paper")){
                    //You Lose
                    Toast.makeText(this,"The Computer picked Paper! You Lose!",Toast.LENGTH_SHORT).show();
                    message = "";
                }
                else if(cpuChoice.equals("Scissors")){
                    //You win
                    Toast.makeText(this,"The Computer picked Scissors! You Win!",Toast.LENGTH_SHORT).show();
                    upScore();
                }
                break;

            case R.id.paper_button:
                if(cpuChoice.equals("Paper")){
                    //Tie
                    Toast.makeText(this,"The Computer picked Paper! It's a Tie!",Toast.LENGTH_SHORT).show();
                    message = "";
                }
                else if(cpuChoice.equals("Scissors")){
                    //You Lose
                    Toast.makeText(this,"The Computer picked Scissors! You Lose!",Toast.LENGTH_SHORT).show();
                    message = "The Computer picked Scissors! You Lose!";
                }
                else if(cpuChoice.equals("Rock")){
                    //You win
                    Toast.makeText(this,"The Computer picked Rock! You Win!",Toast.LENGTH_SHORT).show();
                    message = "The Computer picked Rock! You Win!";
                    upScore();
                }
                break;

            case R.id.scissors_button:
                if(cpuChoice.equals("Scissors")){
                    //Tie
                    Toast.makeText(this,"The Computer picked Scissors! It's a Tie!",Toast.LENGTH_SHORT).show();
                    message = "The Computer picked Scissors! It's a Tie!";
                }
                else if(cpuChoice.equals("Rock")){
                    //You Lose
                    Toast.makeText(this,"The Computer picked Rock! You Lose!",Toast.LENGTH_SHORT).show();
                    message = "The Computer picked Rock! You Lose!";
                }
                else if(cpuChoice.equals("Paper")){
                    //You win
                    Toast.makeText(this,"The Computer picked Paper! You Win!",Toast.LENGTH_SHORT).show();
                    message = "The Computer picked Paper! You Win!";
                    upScore();
                }
                break;

        }

      //  DialogFragment dialog = WinOrLoseDialog.newInstance(message);
      //  dialog.show(getFragmentManager(),"WinOrLose");


    }

    public void upScore(){

        scoreCount++;
        scoreView.setText("Score: " + scoreCount);
    }

}
